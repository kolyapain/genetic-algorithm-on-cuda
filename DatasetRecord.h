#ifndef DATASET_RECORD_H
#define DATASET_RECORD_H

#include <vector>
#include <memory>

template <class T, class R>
class Dataset_Record
{
public:
	Dataset_Record(std::vector<T>* data, std::vector<R>* results);
	~Dataset_Record();
	inline std::shared_ptr<std::vector<R>> get_expected_result() { return expected_result; }
	inline std::shared_ptr<std::vector<R>> get_input_values() { return input_values; }
private:
	std::shared_ptr<std::vector<T>> input_values;
	std::shared_ptr<std::vector<R>> expected_result;
};

#endif // !DATASET_RECORD_H

template <class T, class R>
Dataset_Record<T, R>::Dataset_Record(std::vector<T>* data, std::vector<R>* results)
{
	this->input_values = std::make_shared<std::vector<T>>(data);
	this->expected_result = std::make_shared<std::vector<R>>(results);
}

template <class T, class R>
Dataset_Record<T, R>::~Dataset_Record()
{
	delete input_values;
	delete expected_result;
}