#include "ConfigurationManager.h"

__global__ void addKernel(int *c, int *a, int *b, int offset)
{
	int i = threadIdx.x + offset * thread_count;
	c[i] = a[i] + b[i];
}


/*
Configuration_Manager::Configuration_Manager(const std::string & filename)
{

}
*/