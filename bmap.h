#ifndef BMAP_H_
#define BMAP_H_

#include <map>

template <class K, class V>
class BMap
{
public:
	BMap() {}
	~BMap() {}

	BMap(const BMap& other) = delete;
	BMap(BMap&& other) = delete;
	BMap& operator=(const BMap& other) = delete;
	BMap& operator=(BMap&& other) = delete;

	void add(const K & key, const V & value);
	V get(const K& key);
	K get(const V& value);
protected:
	std::map<K, V> direct_map;
	std::map<V, K> inverse_map;
};

template<class K, class V>
inline void BMap<K, V>::add(const K & key, const V & value)
{
	direct_map[key] = value;
	inverse_map[value] = key;
}

template<class K, class V>
inline V BMap<K, V>::get(const K & key)
{
	auto ptr = this->direct_map.find(key);
	if (ptr == this->direct_map.end())
		throw std::invalid_argument("key not found");
	return ptr->second;
}

template<class K, class V>
inline K BMap<K, V>::get(const V & value)
{
	auto ptr = this->inverse_map.find(value);
	if (ptr == this->inverse_map.end())
		throw std::invalid_argument("value not found");
	return ptr->second;
}

#endif // !BMAP_H_
