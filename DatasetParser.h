#ifndef DATASET_PARSER_H
#define DATASET_PARSET_H

#include <fstream>
#include <string>


class Dataset_Parser
{
public:
	Dataset_Parser(const std::string& file) 
		: filename(file) 
	{

	}

	~Dataset_Parser() 
	{ 
		filereader.close(); 
	}

	int open_file() 
	{ 
		filereader.open(filename, std::ifstream::in);  
		if (!filereader.is_open()) 
			return 1; 
		return 0; 
	}

	std::shared_ptr<std::string> read_string()
	{
		std::string * str = new std::string;
		std::getline(filereader, *str);
		return std::shared_ptr<std::string>(str);
	}

private:
	std::ifstream filereader;
	std::string filename;
};

#endif // !DATASET_PARSER_H
