#include "cuda_functions.h"

namespace ga_cuda
{
	// random unsigned number using state on device
	// size_t id - id of thread
	// curandState * state - pointer on array of all threads states
	__device__ size_t rand_on_device(const size_t id, curandState * state)
	{
		curandState localState = state[id];
		size_t random_ul = curand(&localState);
		// saving new state
		state[id] = localState;
		return random_ul;
	}

	__device__ double rand_double_on_device(const size_t id, curandState* state)
	{
		curandState localState = state[id];
		double random_d = curand_uniform_double(&localState);
		// saving new state
		state[id] = localState;
		return random_d;
	}

	__global__ void generate_population(double* population, const size_t chr_size, const double min_v, const double max_v, curandState* state)
	{
		size_t thread_id = threadIdx.x + blockDim.x*blockIdx.x;
		size_t block_id = thread_id * (chr_size + 1);

		for (size_t i = 0; i < chr_size; ++i)
			population[block_id + i] = (max_v - min_v) * rand_double_on_device(thread_id, state);
	}

	__global__ void rand_ids(curandState *my_curandstate, size_t * ids)
	{
		size_t idx = threadIdx.x + blockDim.x*blockIdx.x;
		float randf = curand_uniform(my_curandstate + idx) * 1000.0;
		ids[idx] = (size_t)randf % 10;
	}

	__global__ void init_kernel(curandState  *state)
	{
		size_t id = threadIdx.x + blockDim.x*blockIdx.x;
		curand_init(1234, id, 0, &state[id]);
	}

	__device__ double calc_fitness(double * chromosome,
		const double* koef,
		const unsigned long* expected_result,
		const unsigned long* sizes,
		const size_t chr_size,
		const size_t dataset_size,
		const size_t max_training_size,
		double * sum,
		unsigned long * chr_result,
		const size_t id,
		const size_t shared_memory_offset)
	{
		unsigned long count = 0;
		unsigned long total = 0;
		unsigned long test_id = 0;

		// for each dataset
		for (size_t i = 0; i < dataset_size; ++i)
		{
			// for each source in dataset calculate rank
			for (size_t j = 0; j < sizes[i]; ++j)
			{
				double tmp = 0.0;
				for (size_t k = 0; k < chr_size; ++k)
					tmp += chromosome[id + k] * koef[total * chr_size + k]; // koef[i][j][k];
				sum[shared_memory_offset + j] = tmp;
				chr_result[shared_memory_offset + j] = j;
				++total;
			}
			// now we've got array of ranks for each book
			// sort it
			// B U B B L E S O R T
			double tmp;
			bool swapped = false;
			do {
				swapped = false;
				for (size_t j = 1; j < sizes[i]; ++j)
					if (sum[shared_memory_offset + j] > sum[shared_memory_offset + j - 1])
					{
						swapped = true;
						tmp = sum[shared_memory_offset + j];
						sum[shared_memory_offset + j] = sum[shared_memory_offset + j - 1];
						sum[shared_memory_offset + j - 1] = tmp;
						unsigned long long tmp_id = chr_result[shared_memory_offset + j];
						chr_result[shared_memory_offset + j] = chr_result[shared_memory_offset + j - 1];
						chr_result[shared_memory_offset + j - 1] = tmp_id;
					}
			} while (swapped);

			// count correct and uncorrect answers
			for (size_t j = 0; j < sizes[i]; ++j)
				if (chr_result[shared_memory_offset + j] == expected_result[test_id + j])
					count += 1;
			test_id += sizes[i];
		}

		return (double)count / (double)total;
	}

	// parallel fitness calculation for each chromosome
	__global__ void fitness(double* chromosome, // matrix of chomosomes and last element of each row is fitness (size + 1)
		const double* koef, // koef - matrix from training data with koefs for ranking
		const unsigned long* expected_result, // expected_result - matrix from training data with sorted by decrease coded sources for calculation of fitness
		const unsigned long* sizes, // sizes - vector with lenght of each dataset
		const size_t chr_size, // chr_size - size of each DNA
		const size_t dataset_size, // dataset_size - size of data
		const size_t max_training_size, // max_training_size - max size of training set (avoid memory allocation for each iteration)
		double * sum, // 
		unsigned long * chr_result//
	)
	{
		// each cuda thread calcuting one sample of DNA
		size_t id = (blockDim.x * blockIdx.x + threadIdx.x) * (chr_size + 1);
		size_t shared_memory_offset = (blockDim.x * blockIdx.x + threadIdx.x) * max_training_size;

		chromosome[id + chr_size] = calc_fitness(chromosome, koef, expected_result, sizes, chr_size, dataset_size, max_training_size, sum, chr_result, id, shared_memory_offset);
	}

	__global__ void tournament(const double* chromosome, // DNAs
		const size_t chr_size, // size of DNA
		const size_t population_size, // size of population
		const size_t players_cnt, // players in match
		double* selected,  // each thread writes here selected (best) chromosomes
		curandState * state // states for random
	)
	{
		// id - is offset is memory for each thread
		size_t stateID = blockDim.x * blockIdx.x + threadIdx.x;
		size_t id = (blockDim.x * blockIdx.x + threadIdx.x) * (chr_size + 1);
		// first random will be the best
		size_t best_id = rand_on_device(stateID, state) % population_size;
		// generating random players and select best
		for (size_t i = 0; i < players_cnt - 1; ++i)
		{
			size_t random_ul = rand_on_device(stateID, state) % population_size;
			if (chromosome[best_id * (chr_size + 1) + chr_size] < chromosome[random_ul * (chr_size + 1) + chr_size])
				best_id = random_ul;
		}
		// save best id in memory
		best_id *= chr_size + 1;
		for (size_t i = 0; i <= chr_size; ++i)
			selected[id + i] = chromosome[best_id + i];
	}

	__global__ void crossover(const double* population, // parents popultaion
		const size_t chr_size, // chromosome size
		const size_t* sorted_ids, // ids of best parents
		const size_t popultaion_size, // size of population
		double* new_popultaion, // new population
		const double* koef, // koef - matrix from training data with koefs for ranking
		const unsigned long* expected_result, // expected_result - matrix from training data with sorted by decrease coded sources for calculation of fitness
		const unsigned long* sizes, // sizes - vector with lenght of each dataset
		const size_t dataset_size, // dataset_size - size of data
		const size_t max_training_size, // max_training_size - max size of training set (avoid memory allocation for each iteration)
		double * sum, // 
		unsigned long * chr_result, //
		double * shared_memory // space for calculations
	)
	{
		size_t thread_id = blockDim.x * blockIdx.x + threadIdx.x;
		size_t id = thread_id * (chr_size + 1);
		size_t shared_memory_id = thread_id * 3 * (chr_size + 1);
		size_t shared_memory_offset = thread_id * max_training_size;

		size_t father = sorted_ids[thread_id / 128] * (chr_size + 1);
		size_t mother = sorted_ids[thread_id % 128 + 1 + thread_id / 128] * (chr_size + 1);
		size_t best[2];

		for (size_t i = 0; i < chr_size; ++i)
		{
			double tmp;
			tmp = (population[father + i] + population[mother + i]) / 2.0;
			if (tmp < 0.0)
				tmp *= -1.0;
			shared_memory[shared_memory_id + i] = tmp;

			tmp = (3 * population[mother + i] - population[father + i]) / 2.0;
			if (tmp < 0.0)
				tmp *= -1.0;
			shared_memory[shared_memory_id + i + (chr_size + 1)] = tmp;

			tmp = (3 * population[father + i] - population[mother + i]) / 2.0;
			if (tmp < 0.0)
				tmp *= -1.0;
			shared_memory[shared_memory_id + i + (chr_size + 1) * 2] = tmp;
		}

		double fitness_1 = calc_fitness(shared_memory, koef, expected_result, sizes, chr_size, dataset_size, max_training_size, sum, chr_result, shared_memory_id, shared_memory_offset);
		double fitness_2 = calc_fitness(shared_memory, koef, expected_result, sizes, chr_size, dataset_size, max_training_size, sum, chr_result, shared_memory_id + (chr_size + 1), shared_memory_offset);
		double fitness_3 = calc_fitness(shared_memory, koef, expected_result, sizes, chr_size, dataset_size, max_training_size, sum, chr_result, shared_memory_id + (chr_size + 1) * 2, shared_memory_offset);

		shared_memory[shared_memory_id + chr_size] = fitness_1;
		shared_memory[shared_memory_id + 2 * chr_size + 1] = fitness_2;
		shared_memory[shared_memory_id + 3 * chr_size + 2] = fitness_3;

		if (fitness_1 < fitness_2)
		{
			best[0] = 1;
			if (fitness_1 < fitness_3)
				best[1] = 2;
			else
				best[1] = 0;
		}
		else
		{
			best[0] = 0;
			if (fitness_2 < fitness_3)
				best[1] = 2;
			else
				best[1] = 1;
		}

		for (size_t i = 0; i < 2; ++i)
		{
			size_t new_popultaion_offset = id + i * (chr_size + 1);
			size_t shared_memory_copy_offset = shared_memory_id + best[i] * (chr_size + 1);
			for (size_t j = 0; j < chr_size; ++j)
				new_popultaion[new_popultaion_offset + j] = shared_memory[shared_memory_copy_offset + j];
			new_popultaion[new_popultaion_offset + chr_size] = shared_memory[shared_memory_copy_offset + chr_size];
		}
	}

	__global__ void mutation(double* population, const size_t chr_size, curandState * state, const double min_v, const double max_v)
	{
		size_t stateID = blockDim.x * blockIdx.x + threadIdx.x;
		size_t id = (blockDim.x * blockIdx.x + threadIdx.x) * (chr_size + 1);

		if (rand_on_device(stateID, state) % 2)
		{
			size_t rand_chromosome = rand_on_device(stateID, state) % chr_size;
			population[id + rand_chromosome] = rand_double_on_device(stateID, state) * (max_v - min_v);
		}
	}

	__global__ void accuracy(const double* population, const size_t chr_size, const size_t count, const size_t block_size, double* results, size_t* max_acc_id)
	{
		size_t thread_id = blockDim.x * blockIdx.x + threadIdx.x;
		size_t id = (blockDim.x * blockIdx.x + threadIdx.x) * (chr_size + 1) * block_size;

		double block_sum = 0.0;
		double max_acc = 0.0;
		size_t max_id = 0;

		for (size_t i = 0; i < block_size; ++i)
		{
			size_t p_id = id + (chr_size + 1) * i + chr_size;
			block_sum += population[p_id];
			if (population[id + chr_size] > max_acc)
			{
				max_acc = population[id + chr_size];
				max_id = id + (chr_size + 1) * i;
			}
				
		}

		results[thread_id] = block_sum / block_size;
		max_acc_id[thread_id] = max_id;
	}
}