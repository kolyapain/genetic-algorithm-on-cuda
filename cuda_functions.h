#ifndef M_CUDA_FUNCTIONS_H
#define M_CUDA_FUNCTIONS_H

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "curand.h"
#include "curand_kernel.h"

namespace ga_cuda
{
	__device__ size_t rand_on_device(const size_t id, curandState * state);


	__device__ double rand_double_on_device(const size_t id, curandState* state);


	__global__ void generate_population(double* population, const size_t chr_size, const double min_v, const double max_v, curandState* state);


	__global__ void rand_ids(curandState *my_curandstate, size_t * ids);


	__global__ void init_kernel(curandState  *state);


	__device__ double calc_fitness(double * chromosome,
		const double* koef,
		const unsigned long* expected_result,
		const unsigned long* sizes,
		const size_t chr_size,
		const size_t dataset_size,
		const size_t max_training_size,
		double * sum,
		unsigned long * chr_result,
		const size_t id,
		const size_t shared_memory_offset);


	__global__ void fitness(double* chromosome, // matrix of chomosomes and last element of each row is fitness (size + 1)
		const double* koef, // koef - matrix from training data with koefs for ranking
		const unsigned long* expected_result, // expected_result - matrix from training data with sorted by decrease coded sources for calculation of fitness
		const unsigned long* sizes, // sizes - vector with lenght of each dataset
		const size_t chr_size, // chr_size - size of each DNA
		const size_t dataset_size, // dataset_size - size of data
		const size_t max_training_size, // max_training_size - max size of training set (avoid memory allocation for each iteration)
		double * sum, // 
		unsigned long * chr_result//
	);


	__global__ void tournament(const double* chromosome, // DNAs
		const size_t chr_size, // size of DNA
		const size_t population_size, // size of population
		const size_t players_cnt, // players in match
		double* selected,  // each thread writes here selected (best) chromosomes
		curandState * state // states for random
	);


	__global__ void crossover(const double* population, // parents popultaion
		const size_t chr_size, // chromosome size
		const size_t* sorted_ids, // ids of best parents
		const size_t popultaion_size, // size of population
		double* new_popultaion, // new population
		const double* koef, // koef - matrix from training data with koefs for ranking
		const unsigned long* expected_result, // expected_result - matrix from training data with sorted by decrease coded sources for calculation of fitness
		const unsigned long* sizes, // sizes - vector with lenght of each dataset
		const size_t dataset_size, // dataset_size - size of data
		const size_t max_training_size, // max_training_size - max size of training set (avoid memory allocation for each iteration)
		double * sum, // 
		unsigned long * chr_result, //
		double * shared_memory // space for calculations
	);


	__global__ void mutation(double* population, const size_t chr_size, curandState * state, const double min_v, const double max_v);


	__global__ void accuracy(const double* population, const size_t chr_size, const size_t count, const size_t block_size, double* results, size_t* max_acc_id);
}

#endif // !M_CUDA_FUNCTIONS_H
