#pragma once

#include <memory>
#include <iostream>
#include <vector>

#include "cuda_functions.h"

#define BLOCK_COUNT 16
#define THREAD_COUNT 512

class Genetic_Algorithm
{
public:
	Genetic_Algorithm(size_t size, size_t chromosomes, double min_v, double max_v, double min_accuracy) noexcept;
	~Genetic_Algorithm();
	double* run();
protected:
	int Generate_population();
	int Tournament();
	int Crossover();
	int Mutation();
	int Selection();
private:
	curandState * d_state;

	// config
	unsigned long max_training_size = 10;
	//shared gpu memory
	double * shared_memory;
	double * sum;
	unsigned long * chr_result;

	double * population;
	double * cuda_population;
	size_t size;
	size_t chromosomes_size;
	double min_v;
	double max_v;

	/// training data
	double * koefs;
	unsigned long * expected_results;
	unsigned long * sizes;
	size_t data_sets;

	double * cuda_koefs;
	unsigned long * cuda_expected_results;
	unsigned long * cuda_sizes;

	// tournament
	double * cuda_tournament_population;
	double * tournament_population;

	// crossiver
	double * crossover_population;
	double * cuda_crossover_population;
	size_t * cuda_sorted_id;

	// assessment
	size_t size_of_selection;
	size_t block_size;
	size_t * cuda_best_accuracy_id;
	double * cuda_selection_accuracy;
	size_t * best_accuracy_id;
	double * best_chromosome;
	double * selection_accuracy;
	double current_accuracy;
	double previous_accuracy;
	double min_accuracy_to_break;
	// preconditions
	int prepare_training_data();
	int init_cuda_kernel();
	int cuda_allocate_memory();
};

/*
data_sets = 2;

	sizes = new unsigned long[data_sets];
	sizes[0] = 3;
	sizes[1] = 2;

	expected_results = new unsigned long[5];

	// set 1
	expected_results[0] = 1;
	expected_results[1] = 0;
	expected_results[2] = 2;

	// set 2
	expected_results[3] = 1;
	expected_results[4] = 0;


	koefs = new double[5 * 3];
	// set 1
	// book 1
	koefs[0] = 0.3;
	koefs[1] = 0.3;
	koefs[2] = 0.4;
	// book 2
	koefs[3] = 0.7;
	koefs[4] = 0.2;
	koefs[5] = 0.1;
	// book 3
	koefs[6] = 0.3;
	koefs[7] = 0.5;
	koefs[8] = 0.2;

	// set 2
	// book 1
	koefs[9] = 0.2;
	koefs[10] = 0.2;
	koefs[11] = 0.6;
	// book 2
	koefs[12] = 0.3;
	koefs[13] = 0.2;
	koefs[14] = 0.5;

*/