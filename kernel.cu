

#include "GeneticAlgorithm.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

int main()
{
	Genetic_Algorithm ga(8192, 4, 0.0, 100.0, 0.989);
	ga.run();
	return 0;
}
