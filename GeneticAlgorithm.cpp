#include "GeneticAlgorithm.h"

Genetic_Algorithm::Genetic_Algorithm(size_t size, size_t chromosomes, double min_v, double max_v, double min_accuracy) noexcept
{
	this->size_of_selection = 128;
	this->block_size = 64;

	this->size = size;
	this->chromosomes_size = chromosomes;
	this->min_v = min_v;
	this->max_v = max_v;
	current_accuracy = 0.0;
	previous_accuracy = 0.0;
	this->min_accuracy_to_break = min_accuracy;

	this->population = new double[size * (chromosomes + 1)];
	this->tournament_population = new double[size * (chromosomes + 1)];
	this->crossover_population = new double[size * (chromosomes + 1)];
	this->best_accuracy_id = new size_t[size_of_selection];
	this->selection_accuracy = new double[size_of_selection];
	this->best_chromosome = new double[chromosomes + 1];
}

Genetic_Algorithm::~Genetic_Algorithm()
{
	if (population != nullptr)
		delete[] population;

	if (tournament_population != nullptr)
		delete[] tournament_population;

	if (crossover_population != nullptr)
		delete[] crossover_population;

	if (best_accuracy_id != nullptr)
		delete[] best_accuracy_id;

	if (selection_accuracy != nullptr)
		delete[] selection_accuracy;

	if (best_chromosome != nullptr)
		delete[] best_chromosome;

	if (cuda_population != nullptr)
		cudaFree(cuda_population);

	if (shared_memory != nullptr)
		cudaFree(shared_memory);

	if (sum != nullptr)
		cudaFree(sum);

	if (cuda_koefs != nullptr)
		cudaFree(cuda_koefs);

	if (cuda_expected_results != nullptr)
		cudaFree(cuda_expected_results);

	if (cuda_sizes != nullptr)
		cudaFree(cuda_sizes);

	if (cuda_tournament_population != nullptr)
		cudaFree(cuda_tournament_population);

	if (cuda_crossover_population != nullptr)
		cudaFree(cuda_crossover_population);

	if (cuda_sorted_id != nullptr)
		cudaFree(cuda_sorted_id);

	if (cuda_best_accuracy_id != nullptr)
		cudaFree(cuda_best_accuracy_id);

	if (cuda_selection_accuracy != nullptr)
		cudaFree(cuda_selection_accuracy);
}

int Genetic_Algorithm::init_cuda_kernel()
{
	cudaError_t status = cudaSetDevice(0);
	if (status != cudaSuccess)
		return 1;

	ga_cuda::init_kernel<<<BLOCK_COUNT, THREAD_COUNT>>>(d_state);

	status = cudaGetLastError();
	if (status != cudaSuccess)
		return 1;

	status = cudaDeviceSynchronize();
	if (status != cudaSuccess)
		return 1;

	return 0;
}

int Genetic_Algorithm::cuda_allocate_memory()
{
	cudaError_t status;

	// allocating vector for population in GPU
	status = cudaMalloc((void**)&cuda_population, (chromosomes_size + 1) * size * sizeof(double));
	if (status != cudaSuccess)
		return 1;

	// allocating states for rand numbers
	status = cudaMalloc((void**)&d_state, size * sizeof(curandState));
	if (status != cudaSuccess)
		return 1;

	// allocating memory for tournament population
	status = cudaMalloc((void**)&cuda_tournament_population, size * (chromosomes_size + 1) * sizeof(double));
	if (status != cudaSuccess)
		return 1;

	// allocating memory for crossover population
	status = cudaMalloc((void**)&cuda_crossover_population, (chromosomes_size + 1) * size * sizeof(double));
	if (status != cudaSuccess)
		return 1;

	// allocating memory for sorted ids
	status = cudaMalloc((void**)&cuda_sorted_id, size * sizeof(size_t));
	if (status != cudaSuccess)
		return 1;

	// allocating shared memory for cuda threads
	status = cudaMalloc((void**)&shared_memory, (chromosomes_size + 1) * size * 3 * sizeof(double));
	if (status != cudaSuccess)
		return 1;

	// allocating memory for sums
	status = cudaMalloc((void**)&sum, size * max_training_size * sizeof(double));
	if (status != cudaSuccess)
		return 1;

	// allocating memory for results of population
	status = cudaMalloc((void**)&chr_result, size * max_training_size * sizeof(unsigned long));
	if (status != cudaSuccess)
		return 1;

	status = cudaMalloc((void**)&cuda_best_accuracy_id, size_of_selection * sizeof(size_t));
	if (status != cudaSuccess)
		return 1;

	status = cudaMalloc((void**)&cuda_selection_accuracy, size_of_selection * sizeof(double));
	if (status != cudaSuccess)
		return 1;

	return 0;
}

double * Genetic_Algorithm::run()
{
	// preconditions
	if (this->cuda_allocate_memory())
		return nullptr;
	if (this->init_cuda_kernel())
		return nullptr;
	if (this->prepare_training_data())
		return nullptr;

	// generate populataion
	if (this->Generate_population())
		return nullptr;

	size_t iteration = 1;
	do 
	{
		// select best
		if (this->Tournament())
			return nullptr;
		// crossing over best parents
		if (this->Crossover())
			return nullptr;
		// mutation
		if (this->Mutation())
			return nullptr;
		// selection
		int code = this->Selection();
		if (code == 1)
			return nullptr;

		if (code != -1)
		{
			std::cout << "iteration : " << iteration << "\nprevious avarage accuracy : " << previous_accuracy << "\ncurrent avarage accuracy : " << current_accuracy << "\nbest chromosome : " << std::endl;
			for (size_t i = 0; i < chromosomes_size; ++i)
				std::cout << best_chromosome[i] << " ";
			std::cout << "\n\twith accuracy : " << best_chromosome[chromosomes_size] << std::endl;
		}

		if (best_chromosome[chromosomes_size] > min_accuracy_to_break)
		{
			std::cout << "\n\n---BEST CHROMOSOME---\n\n";
			for (size_t i = 0; i < chromosomes_size; ++i)
				std::cout << best_chromosome[i] << " ";
			std::cout << "\n\twith accuracy : " << best_chromosome[chromosomes_size] << std::endl;
			break;
		}

	} while (++iteration <= 100);

	return best_chromosome;
}

int Genetic_Algorithm::Generate_population()
{
	ga_cuda::generate_population<<<BLOCK_COUNT, THREAD_COUNT>>>(cuda_population, chromosomes_size, min_v, max_v, d_state);

	cudaError_t status = cudaGetLastError();
	if (status != cudaSuccess)
		return 1;

	status = cudaDeviceSynchronize();
	if (status != cudaSuccess)
		return 1;

	ga_cuda::fitness<<<BLOCK_COUNT, THREAD_COUNT>>>(cuda_population, cuda_koefs, cuda_expected_results, cuda_sizes, chromosomes_size, data_sets, max_training_size, sum, chr_result);

	status = cudaGetLastError();
	if (status != cudaSuccess)
		return 1;

	status = cudaDeviceSynchronize();
	if (status != cudaSuccess)
		return 1;

	return 0;
}

int Genetic_Algorithm::Tournament()
{
	cudaError_t status;

	ga_cuda::tournament<<<BLOCK_COUNT, THREAD_COUNT>>>(cuda_population, chromosomes_size, size, 3, cuda_tournament_population, d_state);

	status = cudaGetLastError();
	if (status != cudaSuccess)
		return 1;

	status = cudaDeviceSynchronize();
	if (status != cudaSuccess)
		return 1;

	return 0;
}

int Genetic_Algorithm::Crossover()
{
	std::vector<size_t> vid;
	vid.reserve(size);
	for (size_t i = 0; i < size; ++i)
		vid.push_back(i);

	// coping tournament population to sort it on cpu
	cudaError_t status = cudaMemcpy(tournament_population, cuda_tournament_population, size * (chromosomes_size + 1) * sizeof(double), cudaMemcpyDeviceToHost);
	if (status != cudaSuccess)
		return 1;

	// sort on host
	bool swapped = false;
	size_t offset = chromosomes_size + 1;
	do
	{
		swapped = false;
		for (size_t i = 1; i < size; ++i)
		{
			if (tournament_population[offset * vid[i] + chromosomes_size] > tournament_population[offset * vid[i - 1] + chromosomes_size])
			{
				std::swap(vid[i], vid[i - 1]);
				swapped = true;
			}
		}
	} while (swapped);

	/*std::cout << std::endl;
	for (size_t i = 0; i < size; ++i)
		std::cout << tournament_population[offset * vid[i] + chromosomes_size] << std::endl;
	std::cout << std::endl;*/

	cudaMemcpy(cuda_sorted_id, vid.data(), size * sizeof(size_t), cudaMemcpyHostToDevice);
	 
	ga_cuda::crossover<<<BLOCK_COUNT, THREAD_COUNT>>>(cuda_tournament_population, chromosomes_size, cuda_sorted_id, size, cuda_crossover_population,
		cuda_koefs, cuda_expected_results, cuda_sizes, data_sets, max_training_size, sum, chr_result, shared_memory);

	status = cudaGetLastError();
	if (status != cudaSuccess)
		return 1;

	status = cudaDeviceSynchronize();
	if (status != cudaSuccess)
		return 1;

	return 0;
}

int Genetic_Algorithm::Mutation()
{
	cudaError_t status;
	
	ga_cuda::mutation<<<BLOCK_COUNT, THREAD_COUNT>>>(cuda_crossover_population, chromosomes_size, d_state, min_v, max_v);

	status = cudaGetLastError();
	if (status != cudaSuccess)
		return 1;

	status = cudaDeviceSynchronize();
	if (status != cudaSuccess)
		return 1;

	ga_cuda::fitness<<<BLOCK_COUNT, THREAD_COUNT>>>(cuda_crossover_population, cuda_koefs, cuda_expected_results, cuda_sizes, chromosomes_size, data_sets, max_training_size, sum, chr_result);

	status = cudaGetLastError();
	if (status != cudaSuccess)
		return 1;

	status = cudaDeviceSynchronize();
	if (status != cudaSuccess)
		return 1;

	/*
	status = cudaMemcpy(crossover_population, cuda_crossover_population, (chromosomes_size + 1) * size * sizeof(double), cudaMemcpyDeviceToHost);
	if (status != cudaSuccess)
		return 1;

	size_t offset = chromosomes_size + 1;

	std::cout << std::endl;
	for (size_t i = 0; i < size; ++i)
	{
		for (size_t j = 0; j < chromosomes_size; ++j)
			std::cout << crossover_population[(chromosomes_size + 1) * i + j] << " ";
		std::cout << " accuracy : " << crossover_population[(chromosomes_size + 1) * i + chromosomes_size] << std::endl;
	}
	std::cout << std::endl;*/

	return 0;
}

int Genetic_Algorithm::Selection()
{
	cudaError_t status;

	ga_cuda::accuracy<<<1, 128>>>(cuda_crossover_population, chromosomes_size, size_of_selection, block_size, cuda_selection_accuracy, cuda_best_accuracy_id);

	status = cudaGetLastError();
	if (status != cudaSuccess)
		return 1;

	status = cudaDeviceSynchronize();
	if (status != cudaSuccess)
		return 1;

	status = cudaMemcpy(best_accuracy_id, cuda_best_accuracy_id, size_of_selection * sizeof(size_t), cudaMemcpyDeviceToHost);
	if (status != cudaSuccess)
		return 1;

	status = cudaMemcpy(selection_accuracy, cuda_selection_accuracy, size_of_selection * sizeof(double), cudaMemcpyDeviceToHost);
	if (status != cudaSuccess)
		return 1;

	status = cudaMemcpy(crossover_population, cuda_crossover_population, (chromosomes_size + 1) * size * sizeof(double), cudaMemcpyDeviceToHost);
	if (status != cudaSuccess)
		return 1;

	double new_accuracy = 0.0;
	size_t max_accuracy_id = 0;
	double max = 0.0;

	for (size_t i = 0; i < size_of_selection; ++i)
	{
		new_accuracy += selection_accuracy[i];
		if (max < crossover_population[best_accuracy_id[i] + chromosomes_size])
		{
			max_accuracy_id = best_accuracy_id[i];
			max = crossover_population[best_accuracy_id[i] + chromosomes_size];
		}
	}

	new_accuracy /= size_of_selection;

	if (best_chromosome[chromosomes_size] < max)
	{
		for (size_t j = 0; j <= chromosomes_size; ++j)
			best_chromosome[j] = crossover_population[max_accuracy_id + j];
	}

	std::cout << std::endl;
	for (size_t i = 0; i < size; ++i)
	{
		for (size_t j = 0; j < chromosomes_size; ++j)
			std::cout << crossover_population[(chromosomes_size + 1) * i + j] << " ";
		std::cout << " accuracy : " << crossover_population[(chromosomes_size + 1) * i + chromosomes_size] << std::endl;
	}
	std::cout << std::endl;

	if (current_accuracy < new_accuracy)
	{
		previous_accuracy = current_accuracy;
		current_accuracy = new_accuracy;

		status = cudaMemcpy(cuda_population, cuda_crossover_population, (chromosomes_size + 1) * size * sizeof(double), cudaMemcpyDeviceToDevice);
		if (status != cudaSuccess)
			return 1;

		return 0;
	}

	return -1;
}

int Genetic_Algorithm::prepare_training_data()
{
	data_sets = 2;

	sizes = new unsigned long[data_sets];
	sizes[0] = 3;
	sizes[1] = 2;

	expected_results = new unsigned long[5];

	// set 1
	expected_results[0] = 1;
	expected_results[1] = 0;
	expected_results[2] = 2;

	// set 2
	expected_results[3] = 1;
	expected_results[4] = 0;


	koefs = new double[5 * chromosomes_size];

	// set 1
	// book 1
	koefs[0] = 0.2;
	koefs[1] = 0.1;
	koefs[2] = 0.4;
	koefs[3] = 0.3;
	// book 2
	koefs[4] = 0.1;
	koefs[5] = 0.6;
	koefs[6] = 0.1;
	koefs[7] = 0.2;
	// book 3
	koefs[8] = 0.5;
	koefs[9] = 0.3;
	koefs[10] = 0.1;
	koefs[11] = 0.1;
	// set 2
	// book 1
	koefs[12] = 0.2;
	koefs[13] = 0.25;
	koefs[14] = 0.25;
	koefs[15] = 0.3;
	// book 2
	koefs[16] = 0.3;
	koefs[17] = 0.3;
	koefs[18] = 0.3;
	koefs[19] = 0.1;

	cudaError_t status;

	status = cudaMalloc((void**)&cuda_koefs, 5 * chromosomes_size * sizeof(double));
	if (status != cudaSuccess)
		return 1;

	status = cudaMemcpy((void*)cuda_koefs, (void*)koefs, 5 * chromosomes_size * sizeof(double), cudaMemcpyHostToDevice);
	if (status != cudaSuccess)
		return 1;

	status = cudaMalloc((void**)&cuda_expected_results, 5 * sizeof(unsigned long));
	if (status != cudaSuccess)
		return 1;

	status = cudaMemcpy((void*)cuda_expected_results, (void*)expected_results, 5 * sizeof(unsigned long), cudaMemcpyHostToDevice);
	if (status != cudaSuccess)
		return 1;

	status = cudaMalloc((void**)&cuda_sizes, 2 * sizeof(unsigned long));
	if (status != cudaSuccess)
		return 1;

	status = cudaMemcpy((void*)cuda_sizes, (void*)sizes, 2 * sizeof(unsigned long), cudaMemcpyHostToDevice);
	if (status != cudaSuccess)
		return 1;

	return 0;
}
